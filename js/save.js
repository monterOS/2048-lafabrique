console.log("Initialisation du jeu");

let arr = {
  "row-1" : {
    "cell-1" : 0,
    "cell-2" : 0,
    "cell-3" : 0,
    "cell-4" : 0
  },

  "row-2" : {
    "cell-5" : 0,
    "cell-6" : 0,
    "cell-7" : 0,
    "cell-8" : 0
  },

  "row-3" : {
    "cell-9" : 0,
    "cell-10" : 0,
    "cell-11" : 0,
    "cell-12" : 0
  },

  "row-4" : {
    "cell-13" : 0,
    "cell-14" : 0,
    "cell-15" : 0,
    "cell-16" : 0
  }
};

let winGame = false;
let loseGame = false;

if (loseGame) {
  console.log("Lose");
}

if (winGame) {
  console.log("Win");
}

/*
 * On passe en argument @i la valeur du for de la fonction @showGrid()
 */
function whatIsMyRow(i) {
  let cell = document.getElementById('cell-'+i);

  if (i > 0 && i < 5)
  {
    return arr['row-1']['cell-'+i];
  }

  if (i > 4 && i < 9) {
    return arr['row-2']['cell-'+i];
  }

  if (i > 8 && i < 13) {
    return arr['row-3']['cell-'+i];
  }

  if (i > 12 && i < 17) {
    return arr['row-4']['cell-'+i];
  }
}


/*
 * Fonction pour afficher l'intégraliter de la grille
 */
function showGrid() {

  for (var i = 1; i < 17; i++) {
    let cell = document.getElementById('cell-'+i);
    cell.innerHTML = whatIsMyRow(i);
  }

}

function takeRowNumber(numberCell) {
  let numberRow;

  if (numberCell == 1 || numberCell == 2 || numberCell == 3 || numberCell == 4) {
    numberRow = 1;
  }

  if (numberCell == 5 || numberCell == 6 || numberCell == 7 || numberCell == 8) {
    numberRow = 2;
  }

  if (numberCell == 9 || numberCell == 10 || numberCell == 11 || numberCell == 12) {
    numberRow = 3;
  }

  if (numberCell == 13 || numberCell == 14 || numberCell == 15 || numberCell == 16) {
    numberRow = 4;
  }

  return numberRow;

}

function percentage2OR4() {
  let percentage = Math.ceil(Math.random() * 100);
  let result;

  if (percentage <= 90) {
    result = 2;
  } else {
    result = 4;
  }
  return result;
}

function cellNumber() {
  let cell = Math.ceil(Math.random() * 16);
  return cell;
}

function insertNumberGrid(cell) {
  return arr['row-'+takeRowNumber(cell)]['cell-'+cell] = percentage2OR4();
}


function randomNumber() {
  let cell = cellNumber();
  let cellValue = arr['row-'+takeRowNumber(cell)]['cell-'+cell];

  if (cellValue === 0) {
    insertNumberGrid(cell);
  }
  else
  {
    checkZero();
  }

}

function resetGrid() {

  for (var i = 1; i < 17; i++) {
    let cell = document.getElementById('cell-'+i);
    cell.className = 'cell';
    arr['row-'+takeRowNumber(i)]['cell-'+i] = 0;
  }

  return arr;

}

/*
 * Fonction pour créer une nouvelle partis afin de recommencer à zéro
 */
function newGame() {
  console.log("Rénitialisation de la grille");
  resetGrid();

  console.log("Insertion des deux numéros dans la grille");
  randomNumber();

  console.log("Affichage de la nouvelle grille");
  showGrid();
}


let btn = document.getElementById('new-game');

btn.addEventListener("click", function (event) {
  event.preventDefault();
  console.log("Lancement d'une partie");
  newGame();
});

window.addEventListener('keydown', function(e) {
  randomNumber();
  showGrid();
});
