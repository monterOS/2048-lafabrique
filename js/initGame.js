/*
 * Fonction pour créer une nouvelle partis afin de recommencer à zéro
 */
function newGame() {
  resetGrid();
  randomNumber();
  showGrid();
}


/*
 * Fonction pour réinitialiser la grille ainsi que les classes CSS des cellules
 * Retourne l'objet arr
 */
function resetGrid() {
  for (var i = 1; i < 17; i++) {
    let cell = document.getElementById('cell-'+i);
    cell.className = 'cell';
    arr['row-'+takeRowNumber(i)]['cell-'+i] = 0;
  }
  return arr;
}



/*
 * Fonction pour choisir aléatoirement la cellule où sera placer les deux nombres pour commencer à jouer
 */
function randomNumber() {
   // Sélection des deux cellules
   let cell1 = cellNumber();
   let cell2 = cellNumber();

   // Si la cellule 1 est égale à la 2 alors on modifie la 2 selon le numéro
   if (cell1 === cell2) {

     if (cell2 === 0) {
       cell2 = (cell2 + 1)
     }

     else if (cell2 === 16) {
       cell2 = (cell2 - 1);
     }

     else {
       cell2 = (cell2 + 1);
     }
   }

   // On insert les deux permiers numéro dans la grille
   insertNumberGrid(cell1);
   insertNumberGrid(cell2);

}


/*
 * Fonction pour tirer aléatoirement la cellule
 * Retourne la variable cell
 */
function cellNumber() {
 let cell = Math.ceil(Math.random() * 16);
 return cell;
}


/*
 * Fonction pour insérer les numéros dans la cellule
 * Argument : On passe en argument le numéro de la cellule
 * Retourne : La modification de l'objet arr
 */
function insertNumberGrid(cell) {
 return arr['row-'+takeRowNumber(cell)]['cell-'+cell] = percentage2OR4();
}


/*
 * Fonction pour sélection la ligne de l'objet arr
 * Argument : On passe en arguement le numéro de la cellules
 * Retourne : Le numéro de la ligne
 */
function takeRowNumber(numberCell) {
 let numberRow;

 if (numberCell == 1 || numberCell == 2 || numberCell == 3 || numberCell == 4) {
   numberRow = 1;
 }

 if (numberCell == 5 || numberCell == 6 || numberCell == 7 || numberCell == 8) {
   numberRow = 2;
 }

 if (numberCell == 9 || numberCell == 10 || numberCell == 11 || numberCell == 12) {
   numberRow = 3;
 }

 if (numberCell == 13 || numberCell == 14 || numberCell == 15 || numberCell == 16) {
   numberRow = 4;
 }

 return numberRow;

}

/*
 * Fonction pour savoir le pourcentage de chance que le 2 soit tirer à la place du 4
 * Retourne : Le résultat du tirage (2 || 4)
 */
function percentage2OR4() {
 let percentage = Math.ceil(Math.random() * 100);
 let result;

 if (percentage <= 90) {
   result = 2;
 } else {
   result = 4;
 }

 return result;
}
