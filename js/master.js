console.log("Init du jeu");

let winGame = false;
let loseGame = false;

let matrix = [[8,0,0,0],
              [0,0,0,0],
              [0,0,0,0],
              [0,0,0,0]];

/**
 * [showGrid description]
 * @return {[type]} [description]
 */
function showGrid() {
  for (let row = 0; row < 4; row++) {
    console.log(matrix[row]);
  }
}

/**
 * [percentage2OR4 description]
 * @return {[type]} [description]
 */
function percentage2OR4() {
  let percentage = Math.floor(Math.random() * Math.floor(100));
  let result;

  if (percentage <= 90) {
    result = 2;
  } else {
    result = 4;
  }
  return result;
}


/**
 * [cellNumber description]
 * @return {[type]} [description]
 */
function cellNumber() {
  let arrReturn = [];

  let row = Math.floor(Math.random() * Math.floor(4));
  let col = Math.floor(Math.random() * Math.floor(4));

  arrReturn = [row, col];

  return arrReturn;
}


function checkGrid() {
  let checkArr = [];

  for (let row = 0; row < 4; row++) {
    for (let col = 0; col < 4; col++) {
      checkArr.push(matrix[row][col]);
    }
  }

  return checkArr;
}

function randomNumber() {
  let cell = cellNumber();
  let cellValue = matrix[cell[0]][cell[1]];

  if (checkGrid().indexOf(0) != -1) {
    if (cellValue === 0) {
      matrix[cell[0]][cell[1]] = percentage2OR4();
    }
    else
    {
      randomNumber();
    }
  }
  else {
    loseGame = true;
    checkGame();
  }
}

function showHtmlGrid() {
  let htmlGrid = checkGrid();

  for (let cell = 0; cell < 16; cell++) {
    let htmlCell = document.getElementById('cell-'+cell);
    htmlCell.innerHTML = htmlGrid[cell];
    htmlCell.className = "cell cell-number-"+htmlGrid[cell];
  }
}

/**
 * [checkGame description]
 * @return {[type]} [description]
 */
function checkGame() {
  if (loseGame) {
    let result = confirm("Vous avez perdu, souhaitez-vous recommencer ?");
    if (result)
    {
      newGame();
      console.log("Nouvelle partie.");
    }
    console.log("Fin de la partie, vous avez perdu");
  }
}

function resetGrid() {

  for (let row = 0; row < 4; row++) {
    for (let col = 0; col < 4; col++) {
      matrix[row][col] = 0;
    }
  }

  return matrix;

}

function newGame() {
  console.log("Réinitialisation de la grille");
  resetGrid();
  console.log("OK");

  console.log("Insertion des deux nombres du début de la partie");
  randomNumber();
  randomNumber();
  console.log("OK");
  console.log("La partie commence !");
}

function convertCol(matrix) {
  let matrixCol = [];
  for (let col = 0; col < 4; col++) {

    matrixCol = [
      [matrix[0][col],
       matrix[1][col],
       matrix[2][col],
       matrix[3][col]],
    ]


  }

  return matrixCol;
}

// Evenement


document.getElementById('new-game').addEventListener('click', function(e) {
  newGame();
  showHtmlGrid();
});


// Mouvement
window.onkeydown = function (event) {

  // Gauche
  if (event.keyCode == 37) {
    // C'est pas jolie mais ça fonctionne (déplacement à gauche)
    for (let row = 0; row < 4; row++) {
      for (let col = 0; col < 4; col++) {
        let cellValue = matrix[row][col];

        let zero = 0;

        if (matrix[row][0] == 0)
        {
          matrix[row][0] = matrix[row][1];
          matrix[row][1] = matrix[row][2];
          matrix[row][2] = matrix[row][3];
          matrix[row][3] = zero;
        }
        else
        {
          if (matrix[row][1] == 0)
          {
            matrix[row][0] = matrix[row][0];
            matrix[row][1] = matrix[row][2];
            matrix[row][2] = matrix[row][3];
            matrix[row][3] = zero;
          }
          else if (matrix[row][2] == 0)
          {
            matrix[row][0] = matrix[row][0];
            matrix[row][1] = matrix[row][1];
            matrix[row][2] = matrix[row][3];
            matrix[row][3] = zero;
          }
        }


      }
    }
  }

  // Droite
  if (event.keyCode == 39) {
    // C'est pas jolie mais ça fonctionne (déplacement à gauche)
    for (let row = 0; row < 4; row++) {
      for (let col = 0; col < 4; col++) {
        let cellValue = matrix[row][col];

        let zero = 0;

        // matrix[row][0] = matrix[row][1];
        // matrix[row][1] = matrix[row][2];
        // matrix[row][2] = matrix[row][3];
        // matrix[row][3] = zero;

        if (matrix[row][3] == 0)
        {
          matrix[row][3] = matrix[row][2];
          matrix[row][2] = matrix[row][1];
          matrix[row][1] = matrix[row][0];
          matrix[row][0] = zero;
        }
        else
        {
          if (matrix[row][2] == 0)
          {
            // matrix[row][0] = matrix[row][0];
            // matrix[row][1] = matrix[row][2];
            // matrix[row][2] = matrix[row][3];
            // matrix[row][3] = zero;

            matrix[row][3] = matrix[row][3];
            matrix[row][2] = matrix[row][1];
            matrix[row][1] = matrix[row][0];
            matrix[row][0] = zero;
          }
          else if (matrix[row][1] == 0)
          {
            // matrix[row][0] = matrix[row][0];
            // matrix[row][1] = matrix[row][1];
            // matrix[row][2] = matrix[row][3];
            // matrix[row][3] = zero;

            matrix[row][3] = matrix[row][3];
            matrix[row][2] = matrix[row][2];
            matrix[row][1] = matrix[row][0];
            matrix[row][0] = zero;

          }
        }


      }
    }
  }

  // Haut
  if (event.keyCode == 38) {
    // C'est pas jolie mais ça fonctionne (déplacement à gauche)
    console.log("Vers le haut");
    let zero = 0;

    let arr =convertCol(matrix);

    console.log(arr);

    // for (let col = 0; col < 4; col++) {
    //
    //   if (matrix[0][col] == 0) {
    //     matrix[0][col] = matrix[1][col];
    //     matrix[1][col] = matrix[2][col];
    //     matrix[2][col] = matrix[3][col];
    //     matrix[3][col] = zero;
    //   } else {
    //     if (matrix[1][col] == 0) {
    //       matrix[0][col] = matrix[0][col];
    //       matrix[1][col] = matrix[2][col];
    //       matrix[2][col] = matrix[3][col];
    //       matrix[3][col] = zero;
    //     } else if (matrix[2][col] == 0) {
    //       matrix[0][col] = matrix[0][col];
    //       matrix[1][col] = matrix[1][col];
    //       matrix[2][col] = matrix[3][col];
    //       matrix[3][col] = zero;
    //     }
    //   }


    //}
  }

  randomNumber();
  showHtmlGrid();
}

// 37 gauche
// 38 haut
// 39 droite
// 40 bas
